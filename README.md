# Ansible Role: Docker


An Ansible Role that installs [Docker](https://www.docker.com) on Linux.

## Requirements

    Centos 7

Docker licence to be copied to
    
    {{ roles }}/ansible-roles-docker/files/commons/docker_subscription.lic
    copy ucp_images_2.2.11.tar.gz and dtr_images_2.4.6.tar.gz to {{ roles }}/ansible-roles-docker/files/commons/

offline packages urls
        
        https://packages.docker.com/caas/ucp_images_2.2.11.tar.gz
        https://packages.docker.com/caas/dtr_images_2.4.6.tar.gz


## Role Variables

    # Credentials for RHEL activation
    # RHEL_VERSION: 7
    # RHEL_USERNAME:
    # RHEL_PASSWORD:
    #
    # # DockerEE daemon URL
    # DOCKER_EE_URL: https://storebits.docker.com/ee/m/sub-xxx-yyy
    #
    # # DockerEE versions
    #
    UCP_VERSION: "2.2.11"
    DTR_VERSION: "2.4.6"
    #
    # # DTR replica id for joining first DTR node
    # REPLICA_ID: 13b873dfa912
    #
    # UCP credentials
    UCP_USERNAME: "administrator"
    UCP_PASSWORD: "@admin123"
    # ---
    
    
    # Edition can be one of: 'ce' (Community Edition) or 'ee' (Enterprise Edition).
    docker_edition: 'ee'
    docker_package: "docker-{{ docker_edition }}"
    docker_package_state: present
    
    # Service options.
    docker_service_state: started
    docker_service_enabled: true
    docker_restart_handler_state: restarted
    
    # Docker Compose options.
    docker_install_compose: false
    docker_compose_version: "1.22.0"
    docker_compose_path: /usr/local/bin/docker-compose
    
    docker_offline_install: "true"
    docker_build_version: "17.06.2.ee.15-3.el7"
    docker_ucp_images: "ucp_images_{{ UCP_VERSION }}.tar.gz"
    docker_dtr_images: "dtr_images_{{ DTR_VERSION ]].tar.gz"

    
    # A list of users who will be added to the docker group.
    docker_users: []

## Dependencies

None.

## Example Playbook

    - hosts: all

        roles:
            - ansible-role-docker

## Inventory Example:
    
    [ucp_managers]
    ucp-master-1 ansible_host=IP1 ucp_leader=1
    ucp-master-2 ansible_host=IP2 ucp_manager=1
    ucp-master-3 ansible_host=IP3 ucp_manager=1
    
    [ucp_workers]
    ucp-worker-1 ansible_host=IP4 dtr_leader=1
    ucp-worker-2 ansible_host=IP5 dtr_replica=1
    ucp-worker-3 ansible_host=IP6 dtr_replica=1
    ucp-worker-4 ansible_host=IP7
    ucp-worker-5 ansible_host=IP8
    
    [ucp:children]
    ucp_managers
    ucp_workers
    
    [dtr:children]
    ucp_workers

    [removed_workers]
    ucp-worker-6 ansible_host=IP9 node_removed=1
